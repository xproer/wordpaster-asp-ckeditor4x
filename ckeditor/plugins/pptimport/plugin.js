﻿CKEDITOR.plugins.add('pptimport',
{
	init: function(editor)
	{
		editor.addCommand('pptimport',
		{
			exec: function(editor)
            {
                WordPaster.getInstance().SetEditor(editor).importPPT();
			}
		});
		editor.ui.addButton('pptimport',
		{
			label: '导入PPT文档',
			command: 'pptimport',
			icon: this.path + 'images/z.png'
		});
	}
});
