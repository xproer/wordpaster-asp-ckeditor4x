﻿CKEDITOR.plugins.add('importword',
{
	init: function(editor)
	{
		editor.addCommand('importword',
		{
			exec: function(editor)
            {
				window.zyOffice.SetEditor(editor).api.openDoc();
			}
		});
		editor.ui.addButton('importword',
		{
			label: '导入Word文档（docx）',
			command: 'importword',
			icon: this.path + 'images/z.png'
		});
	}
});
