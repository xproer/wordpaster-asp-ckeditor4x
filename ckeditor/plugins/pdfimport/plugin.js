﻿CKEDITOR.plugins.add('pdfimport',
{
	init: function(editor)
	{
		editor.addCommand('pdfimport',
		{
			exec: function(editor)
            {
                WordPaster.getInstance().SetEditor(editor).ImportPDF();
			}
		});
		editor.ui.addButton('pdfimport',
		{
			label: '导入PDF文档',
			command: 'pdfimport',
			icon: this.path + 'images/pdf.png'
		});
	}
});
