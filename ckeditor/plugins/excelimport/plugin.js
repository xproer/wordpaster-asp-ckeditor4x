﻿CKEDITOR.plugins.add('excelimport',
{
	init: function(editor)
	{
		editor.addCommand('excelimport',
		{
			exec: function(editor)
            {
				WordPaster.getInstance().SetEditor(editor).importExcel();
			}
		});
		editor.ui.addButton('excelimport',
		{
			label: '导入Excel文档',
			command: 'excelimport',
			icon: this.path + 'images/z.png'
		});
	}
});
